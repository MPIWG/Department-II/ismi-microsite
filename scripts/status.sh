#!/bin/sh

docker compose exec -T -w /var/www/docroot drupal /bin/bash -c 'drush status'
