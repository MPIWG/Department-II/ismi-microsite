#!/bin/bash

# this script needs to run inside the tools container!

set -e

DAILY_DATA=/ismi-data/daily-data/public

# import new database
neo4j-admin database import full \
  --nodes=$DAILY_DATA/neo4j-nodes.csv \
  --relationships=$DAILY_DATA/neo4j-relations.csv \
  --overwrite-destination=true \
  --multiline-fields=true
