# ISMI Drupal microsite

## Installation

```bash
git clone https://gitlab.gwdg.de/MPIWG/Department-II/ismi-microsite.git drupal
cd drupal
mkdir files
mkdir backups
mkdir static-files
cp example.env .env
```

Edit `.env` file to set a new virtual host.
```yml
VIRTUAL_HOST=my.host.com
VIRTUAL_GROUP=mygroup
```

Set a a docker tag
```yml
VERSION=development
```

All variables declared in .env file, could be use as environment variable in docker files.

Multiple domains
```yml
VIRTUAL_HOST=my.host.com,other.host.com
```

## Install docker

Use the init script:
```bash
./ctrl init
```

Or
- install latest docker
- install all requirements
- add user to group docker

## Log in to docker repository
```bash
docker login docker.gitlab.gwdg.de
```
(use Deploy Token created in https://gitlab.gwdg.de/MPIWG/Department-II/ismi-microsite/-/settings/repository)

## Start containers
```sh
docker-compose up -d
```

## Install and configure Drupal

Run install script for the desired *profile* e.g. `ismi`
```sh
./ctrl install ismi
```

## Import existing Drupal instance

To import an existing Drupal instance you need to copy
- the MySQL database (you can use the "dump" and "restore" CTRL commands)
- the file `files/settings.php`
- the contents of `files/files`
- the contents of `static-files`
- the Solr configuration in `configs/solr`

## Update to latest image
```sh
docker-compose pull
docker-compose up -d
```

After an update in the Drupal version or any module you should also run a database update:
```
./ctrl drush updb
```

## CTRL Commands

You can add commands in the scripts folder. The name of the file is the parameter for ctrl.

For example
```sh
./ctrl install minimal
```
calls the script `scripts/install.sh`

### Existing commands

#### Install

```sh
./ctrl install <profile-to-install>
```

#### Status
```sh
./ctrl status
```

#### Update
```sh
./ctrl update
```

#### Drush
```sh
./ctrl drush <command>
```

#### Call drush with docker-compose
```sh
docker-compose exec -w /var/www/docroot drupal drush <command>
```
