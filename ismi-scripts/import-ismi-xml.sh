#!/bin/bash

# this script needs to run inside the drupal container!

#set -e

DRUSH="drush"
FEEDBACK="--feedback=1000 --skip-progress-bar"
COMPOSER_ROOT=/var/www
DOCROOT=/var/www/docroot
DAILY_XML=/ismi-data/daily-data/public
SCRIPTS=/ismi-scripts
if [[ "$1" == "--force-update" ]]
then
    FORCE="update"
elif [[ "$1" == "--force-clean" ]]
then
    FORCE="clean"
elif [[ -n "$1" ]]
then
    echo "ERROR: invalid parameter: $1"
    echo "use: $0 [--force-update|--force-clean]"
    exit 1
fi

ENTITY_LIST='alias codex collection floruit_date misattribution misidentification person place reference repository role study_event subject text transfer_event witness digitalization'

# send errors to sentry
export SENTRY_DSN='https://99890d4844ea41c48f13e21bdb5358db@sentry.mpiwg-berlin.mpg.de/6'
eval "$($SCRIPTS/sentry-cli bash-hook)"

#
# check for new data and a working installation 
# exits if not
#
function check_run () 
{
    # check for new data
    if cmp -s $DAILY_XML/openmind-data.xml $DOCROOT/xml/openmind-data.xml
    then
        echo "No change in OpenMind data! Exiting."
        exit 0
    fi
    
    echo "copy new openmind XML dump ($( date ))"
    mkdir -p $DOCROOT/xml
    cp $DAILY_XML/*.xml $DOCROOT/xml/
    
    # check installation
    cd $COMPOSER_ROOT
    echo "checking drupal installation"
    
    if composer show -q drupal/migrate_plus
    then
        echo "  ok: migrate_plus exists"
    else
        composer install --dev || exit 1
        drush cr
    fi
}

#
# prepare import by setting maintenance mode and disabling index
#
function prepare_import () 
{
    cd $DOCROOT
    
    if [[ -n "$FORCE" ]]
    then
        echo "set maintenance mode ($( date ))"
        drush mset 1
        # clear cache to make sure maintenance mode is displayed
        drush cr
    
        echo "disable search index ($( date ))"
        drush sapi-dis ismi_default_index
    fi
}

#
# import data
#
function import_data_relations () 
{
    if [[ "$FORCE" == "clean" ]]
    then
        echo "delete and re-import entities ($( date ))"
        for ENT in $ENTITY_LIST
        do
            echo "  delete $ENT"
            drush entity:delete node --bundle=$ENT
            # drop migrate tables
            drush sql:query "drop table if exists migrate_map_node_$ENT;"
            drush sql:query "drop table if exists migrate_message_node_$ENT;"
            echo "  re-import $ENT"
            drush migrate:import "node_$ENT" $FEEDBACK
        done
        
        echo "cleanup relations ($( date ))"
        drush sql:query 'drop table if exists migrate_map_all_node_relations;'
        drush sql:query 'drop table if exists migrate_message_all_node_relations;'
        
        echo "import entity relations ($( date ))"
        drush migrate:import all_node_relations $FEEDBACK
    
    elif [[ "$FORCE" == "update" ]]
    then
        echo "update entities ($( date ))"
        for ENT in $ENTITY_LIST
        do
            echo "  update $ENT"
            drush migrate:import "node_$ENT" --sync --update $FEEDBACK
        done
        
        echo "re-import entity relations ($( date ))"
        drush migrate:idmap-table-destroy all_node_relations
        drush migrate:import all_node_relations $FEEDBACK
    
    else
        echo "sync entities ($( date ))"
        for ENT in $ENTITY_LIST
        do
            echo "  sync $ENT"
            drush migrate:import "node_$ENT" --sync $FEEDBACK
        done
        
        echo "sync entity relations ($( date ))"
        drush migrate:import all_node_relations --sync $FEEDBACK
        
    fi
}

#
# update biographies and references in Drupal
#
function update_bios_refs () 
{
  echo "update biographies ($( date ))"
  # must run with --update
  drush migrate:import update_biography --update --feedback=1000 --skip-progress-bar
  
  echo "update references ($( date ))"
  # must run with --update
  drush migrate:import update_reference --update --feedback=1000 --skip-progress-bar
}

#
# enable search index and disable maintenance mode
#
function finish_update () 
{
    if [[ -n "$FORCE" ]]
    then
        echo "re-enable search index ($( date ))"
        drush sapi-en ismi_default_index
    
        echo "start indexing ($( date ))"
        drush sapi-i ismi_default_index
    
        IDX_STAT=$( drush sapi-s ismi_default_index | grep -oE '[0-9.]+%' )
        if [[ "$IDX_STAT" != '100%' ]]
        then
            echo "index at $IDX_STAT not done. retrying..."
            drush sapi-i ismi_default_index
        fi
    
        echo "disable maintenance mode ($( date ))"
        drush sset system.maintenance_mode 0
    fi
}

#
# check messages from import and number of entities
#
function check_import () 
{
    # check for unprocessed fields
    ERRORS=""
    drush ms --group=ismi_migration --format=string --fields=unprocessed,id | while read CNT ID 
    do
        if [[ "$CNT" != "0" ]]
        then
            ERRORS="$ID:$CNT $ERRORS"
        fi
    done
    if [[ "$ERRORS" != "" ]]
    then
        echo "ERRORS while importing! Unprocessed: $ERRORS"
        $SCRIPTS/sentry-cli send-event -m "ERRORS while importing! Unprocessed: $ERRORS"
        exit 1
    fi
    
    # check for relation import messages
    drush ms all_node_relations --format=string --fields=message_count,id | while read CNT ID 
    do
        if [[ "$CNT" != "0" ]]
        then
            echo "WARNING while importing relations! Messages: $CNT"
            $SCRIPTS/sentry-cli send-event -m "WARNING while importing relations! Messages: $CNT"
        fi
    done
    
    # check entity counts
    ERRORS=""
    for ent in $ENTITY_LIST
    do
        echo "check count of entity type $ent"
        # entity count from XML
        num_xml=$( head -c95 "$DOCROOT/xml/openmind-data-$ent.xml" |sed -r -e '1d;2s/.* count="([0-9]+)".*/\1/' )
        # entity count from drupal
        num_drupal=$( drush views:execute count_entities count_entities_csv $ent | tail -1 )
        if [[ $num_xml != $num_drupal ]] 
        then
            echo "count error for entity $ent! (xml: $num_xml, drupal: $num_drupal)"
            ERRORS="$ent(xml:$num_xml, drupal:$num_drupal) $ERRORS"
        fi
    done
    
    if [[ "$ERRORS" != "" ]]
    then
      return 1
    fi
}


##
## main
##

echo "Start at $( date ) $FORCE"
# check if we should run
check_run
# prepare
prepare_import
# import data
import_data_relations
# check import
if check_import
then
    # import ok
    update_bios_refs
    finish_update
else
    # import not successful
    echo "ERRORS counting entities! $ERRORS"
    if [[ "$FORCE" == "" ]]
    then
        # retry as clean import
        FORCE="clean"
        echo "Re-running full import at $( date ) $FORCE"
        prepare_import
        import_data_relations
        check_import
    fi
    
    if [[ "$ERRORS" == "" ]]
    then
        # import ok
        update_bios_refs
        finish_update
    else
        $SCRIPTS/sentry-cli send-event -m "ERRORS counting entities! $ERRORS"
        exit 1
    fi
fi

echo "Done at $( date )"
